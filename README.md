# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## 安装依赖  
`npm install || yarn`  
## 运行  
`npm run dev || yarn dev`  
## 打包  
`npm run build || yarn build`  

## 本地预览  
`npm run preview || yarn preview`  

## 📌 TODO  
* [x] 支持选择文件转换  
* [x] 支持拖拽文件转换  
* [x] 支持粘贴文件转换  
* [x] 支持截屏粘贴转换  

## 线上体验  
访问:http://base64.poshir.top 查看

## 支持  
感谢 [本期节目](https://segmentfault.com/a/1190000013298317 "本期节目")