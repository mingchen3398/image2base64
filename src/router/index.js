import { createRouter, createWebHistory } from 'vue-router'


const Router = createRouter({
    history: createWebHistory(),
    routes: [
    {
        path: '/',
        name: 'img',
        component: () => import('@/views/Upload.vue'),
        meta:{
            keepAlive:true
        }
    }]
})

export default Router
