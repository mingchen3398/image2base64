const createId = () => String(Math.random()).slice(2, 8)
export {
    createId
}