import { createStore } from 'vuex'

const store = createStore({
  state: {
    SYSTEM_INFO: "null"
  },
  getters: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {

  }
})

export default store
